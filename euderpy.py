"""
    euderpy - a bad pun on Euterpe, muse of music. Pronounced "You derpy".

    Currently only single-channel FIFOs with 16 bit resolution supported.
"""

from sys import stdout, exit
from threading import Thread
from struct import unpack

import pyglet
import numpy as np

from scipy.signal import gaussian, hann

VERBOSE = True

# Messing with these settings might mess with performance.
FIFO_FILE = '/tmp/musicpd.fifo'
FREQ = 44100 # Not currently used
SAMPLE_MIN = -32768
SAMPLE_MAX = 32767
WINDOW_SIZE = 4096 # The number of samples used to build a spectrum, also the size of the Collector
READ_SIZE = 2 # How many samples to read at once, make this WINDOW_SIZE if any problems occur

RES_X = 1900
RES_Y = 1050

def bin_ranges(length, num_bins):

        length = np.float64(length)
        bin_range = []
        current_range = length / (2 ** num_bins -1)
        
        lower = 0
        upper = current_range -1
        if upper < lower:
            upper = lower
        bin_range.append([int(round(lower)), int(round(upper))])

        for i in range(1, num_bins):

            current_range *=2

            lower = upper + 1
            upper = lower + current_range - 1
            if upper < lower:
                upper = lower

            bin_range.append([int(round(lower)), int(round(upper))])

        return bin_range


class Window(pyglet.window.Window):

    def __init__(self, *args, **kw):
        super(Window, self).__init__(*args, **kw)
        pyglet.gl.glEnable(pyglet.gl.GL_TEXTURE_2D)
        pyglet.gl.glEnable(pyglet.gl.GL_BLEND)
        pyglet.gl.glDisable(pyglet.gl.GL_DEPTH_TEST)
        pyglet.gl.glBlendFunc(pyglet.gl.GL_SRC_ALPHA, pyglet.gl.GL_ONE_MINUS_SRC_ALPHA)

class Analyzer(list):

    std = None
    filled = None
    amplitudes = None

    def __init__(self, *args, **kw):

        if kw.has_key('std'):
            self.std = kw.pop('std')
        else:
            self.std = False

        self.max = 0 #TODO: Killme
        self.filled = False

        self.amplitudes = np.zeros(int(WINDOW_SIZE/4))

        super(Analyzer, self).__init__(*args, **kw)

    def append(self, item):

        if len(self) == WINDOW_SIZE:
            self.pop(0)

        elif len(self) == (WINDOW_SIZE - 1):
            self.filled = True

        super(Analyzer, self).append(item)


    def get_snapshot(self):

        return list(self)

    
    def update(self):

        frame = self.get_snapshot()

        if self.std:
            frame = np.array(frame) * gaussian(len(frame), self.std)
        else:
            frame = np.array(frame) * hann(len(frame))

        fft = np.abs(np.fft.rfft(frame))
        n = fft.size
        
        try:
            #tm = np.max(fft[0:n/2])
            #if tm > self.max:
            #    self.max = tm
            #    print self.max
            #amplitudes = fft[0:n/2] / self.max
            amplitudes = fft[0:n/2] / 3200000

        except Exception as e:
            print fft
            exit("Whoopsie!")

        clean_amplitudes = []

        for amplitude in amplitudes:

            if not np.isnan(amplitude):
                clean_amplitudes.append(amplitude)

            else:
                print "NAN in fft!"
                clean_amplitudes.append(np.float64(0))

        self.amplitudes = np.array(clean_amplitudes)


    def get_amplitudes(self):


        return self.amplitudes


    def get_amplitudes_in_bins(self, num_bins):

        amplitudes = self.amplitudes
        
        bins = np.zeros(num_bins)
        i = 0
        ranges = bin_ranges(len(amplitudes), num_bins)
        for lower, upper in ranges: 

            if lower == upper:
                bin = amplitudes[lower]

            else: 
                bin = amplitudes[lower:upper].mean()

            if np.isnan(bin):
                bin = np.float64(0)

            bins[i] = bin
            i += 1

        return bins



class Collector(Thread):

    stop_signal = None
    analyzer = None

    def __init__(self, analyzer, interval=None):

        super(Collector, self).__init__()

        self.analyzer = analyzer
        self.daemon = True
        self.stop_signal = False


    def run(self):

        handle = open(FIFO_FILE, 'r')

        while not self.stop_signal:

            chunk = handle.read(READ_SIZE * 2)
            raw_samples = [chunk[i:i+2] for i in range(0, READ_SIZE)]

            for raw_sample in raw_samples:
                self.analyzer.append(unpack('>h', raw_sample)[0])

    def stop(self):

        self.stop_signal = True



class Visualizer(object):

    window = None
    analyzer = None
    smoothed_amplitudes = None

    def __init__(self, window, analyzer):
        self.window = window
        self.analyzer = analyzer

        amps = []
        self.smoothed_amplitudes = np.zeros(int(WINDOW_SIZE/4))

    def update(self):
        if self.analyzer.filled:

            amplitudes = self.analyzer.get_amplitudes()
            self.smoothed_amplitudes = (self.smoothed_amplitudes * 0.95) + (amplitudes * 0.05)
            frame = self.smoothed_amplitudes

            vertex_coords = []
            x = 20
            for sample in frame:
                vertex_coords.extend(
                    (   x,   0,
                        x+1, 0,
                        x+1, sample * 300,
                        x,   sample * 300
                    )
                )
                x += 2

            pyglet.graphics.draw(len(frame)*4, pyglet.gl.GL_QUADS, ('v2f', vertex_coords))



class Background(Visualizer):

    def __init__(self, *args, **kw):

        super(Background, self).__init__(*args, **kw)
        self.smoothed_amplitudes = np.zeros(3)

    def update(self):

        amplitudes = self.analyzer.get_amplitudes_in_bins(3)
        self.smoothed_amplitudes = self.smoothed_amplitudes * 0.9 + amplitudes * 0.1

        vertex_coords = [
            0, 0,
            RES_X, 0,
            RES_X, RES_Y,
            0, RES_Y
        ]

        vertex_colors = []
        vertex_colors += [self.smoothed_amplitudes[0], self.smoothed_amplitudes[1], self.smoothed_amplitudes[2]]
        vertex_colors += [self.smoothed_amplitudes[0], self.smoothed_amplitudes[1], self.smoothed_amplitudes[2]]
        vertex_colors += [self.smoothed_amplitudes[0], self.smoothed_amplitudes[1], self.smoothed_amplitudes[2]]
        vertex_colors += [self.smoothed_amplitudes[0], self.smoothed_amplitudes[1], self.smoothed_amplitudes[2]]

        pyglet.graphics.draw(len(vertex_coords)/2, pyglet.gl.GL_QUADS,
            ('v2f', vertex_coords),
            ('c3f', vertex_colors)
        )




class Chomp(Visualizer):

    rows = None
    cols = None
    num_bins = None
    colors = None
    row_smoothing = None

    def __init__(self, window, analyzer, rows = 3, cols=8):
        super(Chomp, self).__init__(window, analyzer)
        
        self.rows = rows
        self.cols = cols
        self.smoothed_amplitudes = np.zeros((rows,cols))
        self.distance = 0
        self.row_smoothing = [0.9, 0.75, 0.6]

        self.colors = [
            [1,0,0],
            [0,1,0],
            [0,0,1]
        ]


    def update(self):

        if self.analyzer.filled:

            num_bins = self.rows * self.cols

            amplitudes = self.analyzer.get_amplitudes_in_bins(num_bins)
            #self.smoothed_amplitudes = self.smoothed_amplitudes * 0.9 + amplitudes * 0.1
            for r in range(0,self.rows):
                amp_range = (0 + r * self.cols, self.cols + r*self.cols)
                self.smoothed_amplitudes[r] = (
                    self.smoothed_amplitudes[r] * self.row_smoothing[r] +
                    amplitudes[amp_range[0]:amp_range[1]] * (1- self.row_smoothing[r])
                )

            x_left = 20
            x = x_left
            y_bottom = 20 + self.rows * (self.distance +1)

            vertex_coords = []
            vertex_colors = []
            #for amplitude in self.smoothed_amplitudes:
            for row, row_amps in enumerate(self.smoothed_amplitudes):

            
                col = 0

                for col, amplitude in enumerate(row_amps):

                    y_top = RES_Y * amplitude

                    vertex_coords.extend(
                        (   x, y_bottom,
                            x+20, y_bottom,
                            x+20, y_top,
                            x, y_top
                        )
                    )

                    color = self.colors[row] + [amplitude] #*((row+1.5)/np.float64(2))]
                    vertex_colors += color
                    vertex_colors += color
                    vertex_colors += color
                    vertex_colors += color

                    x += 22

                x = x_left + row * 4
                y_bottom -= self.distance
            
            pyglet.graphics.draw(len(vertex_coords)/2, pyglet.gl.GL_QUADS,
                ('v2f', vertex_coords),
                ('c4f', vertex_colors)
            )
#            pyglet.gl.glRotatef(33.3, 0,0,0)


class Roll(Visualizer):

    def update():
        pass



class Triangle(Visualizer):

    smoothing = None

    def __init__(self, window, analyzer):

        super(Triangle, self).__init__(window, analyzer)
        self.smoothed_amplitudes = np.zeros(3)
        self.smoothing = ((0.99, 0.01), (0.9, 0.1), (0.5, 0.5))

    def update(self):

        amps = self.analyzer.get_amplitudes_in_bins(3)
        for i, amp in enumerate(amps):
            self.smoothed_amplitudes[i] = self.smoothed_amplitudes[i] * self.smoothing[i][0] + amp * self.smoothing[i][1]

        print "\r", self.smoothed_amplitudes

        vertex_coords = []
        vertex_colors = []

        pyglet.graphics.draw(len(vertex_coords)/2, pyglet.gl.GL_QUADS,
            ('v2f', vertex_coords),
            ('c3f', vertex_colors)
        )





analyzer = Analyzer(std=WINDOW_SIZE/16)
collector = Collector(analyzer)
collector.start()


config = pyglet.gl.Config(alpha_size=8)
config = pyglet.gl.Config(double_buffer=True, alpha_size=8)
#config = pyglet.gl.Config(alpha_size=8, accum_alpha_size=8)
#config = pyglet.gl.Config(depth_size=32)
window = Window(RES_X, RES_Y, config=config)
bg = Background(window, analyzer)
#v1 = Visualizer(window, analyzer)
v2 = Chomp(window, analyzer, cols=85)
#v3 = Triangle(window, analyzer)


def update(dt):
    analyzer.update()
    window.clear()
    bg.update()
 #   v1.update()
    v2.update()
#    v3.update()

    stdout.write("\r%.2f / %.2f fps" % (clock.get_fps(), clock.get_fps_limit()))


clock = pyglet.clock.get_default()
pyglet.clock.schedule(update)

pyglet.app.run()
